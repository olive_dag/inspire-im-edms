﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventRSVP.Models
{
    public class RegistrationModel
    {
        public int EventId { get; set; }
        public String EventTitle { get; set; }
        public String EventCompany { get; set; }
        public String EventEmail { get; set; }

        [Display(Name = "Salutation")]
        public string Salutation { get; set; }

        [Required(ErrorMessage="Required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        //[Required(ErrorMessage = "Required")]
        //[Display(Name = "Company Size")]
        //public string CompanySize { get; set; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        //[Required(ErrorMessage = "Required")]
        //[Display(Name = "Job Role")]
        //public string JobRole { get; set; }

        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage="Invalid Format")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Contact")]
        public string Contact { get; set; }

        public bool HasParking { get; set; }
        public bool Agree { get; set; }

        public IList<String> Salutations { get; set; }
        public IList<String> CompanySizes { get; set; }
        public IList<String> JobRoles { get; set; }

        public RegistrationModel()
        {
            Salutations = new List<String>();
            Salutations.Add("Mr");
            Salutations.Add("Mrs");
            Salutations.Add("Ms");

            CompanySizes = new List<String>();
            CompanySizes.Add("<500");
            CompanySizes.Add("501-1000");
            CompanySizes.Add("1001-5000");
            CompanySizes.Add("5001-10,000");
            CompanySizes.Add(">10,000");

            JobRoles = new List<String>();
            JobRoles.Add("Logistics/Distribution");
            JobRoles.Add("Procurement");
            JobRoles.Add("Sales & Marketing");
            JobRoles.Add("Executive Management");
            JobRoles.Add("Warehouse Management");
            JobRoles.Add("Inventory/Materials Management");
            JobRoles.Add("Workplace Safety");
            JobRoles.Add("Security & Compliance");
            JobRoles.Add("IT");
            JobRoles.Add("Others");
        }
    }
}