﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventRSVP.Models
{
    public class LoginModel
    {
        [Required]
        public String PasswordLogin { get; set; }
        public String Title { get; set; }
        public int EventId { get; set; }
    }
}