﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventRSVP.Models
{
    public class EventModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name="Event Title")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Company Email")]
        public string CompanyEmail { get; set; }
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

}