﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EventRSVP.Models;

namespace EventRSVP.Controllers
{
    public class EventController : Controller
    {
        private RSVPModelContainer db = new RSVPModelContainer();

        //
        // GET: /Event/

        public ActionResult Login()
        {
            var loggedin = Session["LoggedIn"];
            if (loggedin != null)
            {
                if (loggedin.ToString() == System.Configuration.ConfigurationManager.AppSettings["EventsPassword"])
                {
                    return View("Index", db.Events.ToList());
                }
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.PasswordLogin == System.Configuration.ConfigurationManager.AppSettings["EventsPassword"])
                {
                    Session["LoggedIn"] = model.PasswordLogin;
                    return View("Index", db.Events.ToList());
                }
            }
            ModelState.AddModelError("PasswordLogin", "Incorrect Password");
            return View();
        }

        //
        // GET: /Event/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Event/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EventModel eventt)
        {
            if (ModelState.IsValid)
            {
                Event e = new Event();
                e.Title = eventt.Title;
                e.CompanyEmail = eventt.CompanyEmail;
                e.CompanyName = eventt.CompanyName;
                e.Password = eventt.Password;
                
                db.Events.Add(e);
                db.SaveChanges();
                return RedirectToAction("Login");
            }

            return View(eventt);
        }

        //
        // GET: /Event/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Event eventt = db.Events.Find(id);
            if (eventt == null)
            {
                return HttpNotFound();
            }
            return View(eventt);
        }

        //
        // POST: /Event/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Event eventt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eventt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Login");
            }
            return View(eventt);
        }

        //
        // GET: /Event/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Event eventt = db.Events.Find(id);
            if (eventt != null)
            {
                eventt.Registrations.Clear();

                var registrationsByEvent = db.Registrations.Include(r => r.Event).Where(r => r.EventId == id);
                foreach (Registration r in registrationsByEvent)
                {
                    db.Registrations.Remove(r);
                }

                db.Events.Remove(eventt);
                db.SaveChanges();
            }
            return RedirectToAction("Login");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}