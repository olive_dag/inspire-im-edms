﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EventRSVP.Models;

namespace EventRSVP.Controllers
{
    public class RegistrationController : Controller
    {
        private RSVPModelContainer db = new RSVPModelContainer();

        //
        // GET: /Registration/

        public ActionResult ThankYou(RegistrationModel model)
        {
            return View(model);
        }
        
        public ActionResult Login(int? EventID)
        {
            LoginModel model = new LoginModel();
            if (EventID.HasValue)
            {
                Event eventt = db.Events.Find(EventID.Value);
                if (eventt != null)
                {
                    model.EventId = eventt.Id;
                    model.Title = eventt.Title;
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            Event eventt = db.Events.Find(model.EventId);

            if (ModelState.IsValid)
            {
                if ((eventt!=null) && (eventt.Password == model.PasswordLogin))
                {
                    var registrationsByEvent = db.Registrations.Include(r => r.Event).Where(r => r.EventId == model.EventId);
                    ViewBag.EventTitle = eventt.Title;
                    return View("Index", registrationsByEvent.ToList());
                }
                else
                {
                    ViewBag.EventTitle = eventt.Title;                    
                    ModelState.AddModelError("PasswordLogin", "Incorrect password.");
                }
            }

            model.Title = eventt.Title;
            return View(model);
        }

        //
        // GET: /Registration/Create

        public ActionResult Register(int? EventID)
        {
            RegistrationModel model = new RegistrationModel();

            if (EventID.HasValue)
            {
                var eventt = db.Events.Find(EventID.Value);
                model.EventId = EventID.Value;
                model.EventTitle = eventt.Title;
                model.EventCompany = eventt.CompanyName;
                model.EventEmail = eventt.CompanyEmail;
            }
            ViewBag.EventId = new SelectList(db.Events, "Id", "Title");
            return View(model);
        }

        //
        // POST: /Registration/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                Registration registration = new Registration();
                registration.EventId = model.EventId;
                registration.Salutation = model.Salutation;
                registration.FirstName = model.FirstName;
                registration.LastName = model.LastName;
                registration.CompanyName = model.CompanyName;
                registration.CompanySize = "";
                registration.JobTitle = model.JobTitle;
                registration.JobRole = "";
                registration.Email = model.Email;
                registration.Contact = model.Contact;
                registration.HasParking = model.HasParking;
                registration.Agree = model.Agree;

                db.Registrations.Add(registration);
                db.SaveChanges();
                ViewBag.EventId = model.EventId;
                return View("ThankYou", model);
            }

            ViewBag.EventId = model.EventId;
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}